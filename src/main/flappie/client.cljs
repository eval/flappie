(ns flappie.client
  (:require [flappie.clients.unleash :as unleash]
            [flappie.flag :as flag]))


(defprotocol IClient
  (-init [adapter client config]))

(def local
  (reify
    IClient
    (-init [_adapter client config]
      (merge client config))

    flag/IFlag
    (-fetch-all [_adapter {:local/keys [flags] :as _client} {:keys [update-flags-fn]}]
      (update-flags-fn flags))
    (-enabled? [_adapter client flag]
      (get @(:flags client) flag))))


(def unleash
  (reify
    IClient
    (-init [_adapter client {:unleash/keys [url app-name instance-id]}]
      (merge client #:unleash{:url          url
                              :app-name     app-name
                              :instance-id  instance-id
                              :fetch-status (atom :ready)}))
    flag/IFlag
    (-fetch-all [_adapter client opts]
      (unleash/fetch client opts))
    (-enabled? [_adapter {:keys [flags] :as _client} flag]
      (:enabled (get @flags flag)))))


(comment

  )
