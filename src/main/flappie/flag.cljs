(ns flappie.flag)

(defprotocol IFlag
  (-fetch-all [adapter client opts])
  (-enabled? [adapter client flag-name]))
