(ns flappie.utils)


(defn index-by
  "Returns a map of the elements of coll keyed by the result of f on each
  element. The value at each key will be the last element in coll associated
  with that key. This function is similar to `clojure.core/group-by`, except
  that elements with the same key are overwritten, rather than added to a
  vector of values."
  {:source "https://github.com/weavejester/medley/blob/1.3.0/src/medley/core.cljc#L245"}
  [f coll]
  (persistent! (reduce #(assoc! %1 (f %2) %2) (transient {}) coll)))


(defn map-vals [f m]
  (into (empty m)
        (map (fn [[k v]] [k (f v)])
             m)))


(defn map-keys [f m]
  (into (empty m)
        (map (fn [[k v]] [(f k) v])
             m)))
