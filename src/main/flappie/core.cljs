(ns flappie.core
  (:require [flappie.client :as client]
            [flappie.flag :as flag]))

(def clients {:local   client/local
              :unleash client/unleash})


(goog-define version "")


(defn client
  "Yields client "
  [{:keys [adapter refresh-interval-ms] :as config}]
  (let [adapter         (get clients adapter adapter)
        client          {:adapter adapter
                         :flags   (atom {})}
        client          (client/-init adapter client (dissoc config :adapter))
        update-flags-fn (fn [new-flags]
                          (update client :flags #(reset! % new-flags)))
        fetch-fn        #(flag/-fetch-all adapter % {:update-flags-fn update-flags-fn})]
    (cond-> client
      refresh-interval-ms (assoc :fetcher (js/setInterval #(fetch-fn client) refresh-interval-ms))
      :always             (doto (fetch-fn)))))


(defn enabled?
  "does it flag?"
  [{:keys [adapter] :as client} flag]
  (flag/-enabled? adapter client flag))
