(ns flappie.clients.unleash
  (:require [clojure.string :as str]
            [flappie.utils :as utils]
            [goog.net.XhrIo :as xhr]))

(defn- qualify-flag-name [s]
  (-> s
      (str/replace "__dot__" ".")
      (str/replace "__slash__" "/")))


(defn fetch [{:unleash/keys [app-name instance-id url fetch-status] :as _client}
             {:keys [update-flags-fn]}]
  (let [timeout-ms   4000
        features-url (str url "/features")
        headers      (clj->js {"UNLEASH-APPNAME"    app-name
                               "UNLEASH-INSTANCEID" instance-id})
        status!      #(reset! fetch-status %)
        ready?       (= :ready @fetch-status)]
    (when ready?
      (try
        (status! :pending)
        (xhr/send features-url
                  #(let [resp (.-target %)]
                     (status! :ready)
                     (when (.isSuccess resp)
                       (when-let [json-resp (js->clj (.getResponseJson resp) :keywordize-keys
                                                     true)]
                         (->> (:features json-resp)
                              (utils/index-by :name)
                              (utils/map-keys (comp keyword qualify-flag-name))
                              update-flags-fn))))
                  "GET"
                  #js{}
                  headers
                  timeout-ms)
        (catch js/Exception _e
          (status! :ready))))))
