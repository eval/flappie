(ns simple.core
  (:require [flappie.core :as flap]))


(goog-define UNLEASH-URL "")
(goog-define UNLEASH-APP-NAME "")
(goog-define UNLEASH-INSTANCE-ID "")


(defonce unleash-client
  (flap/client {:adapter             :unleash
                :refresh-interval-ms 10e3
                :unleash/url         UNLEASH-URL
                :unleash/app-name    UNLEASH-APP-NAME
                :unleash/instance-id UNLEASH-INSTANCE-ID}))

(defonce local-client
  (flap/client {:adapter          :local
                :local/flags {:foo true}}))

(defn main! []
  (println "hello!"))

(comment

  (flap/enabled? local-client :foo2)

  )
